﻿namespace wfaTestWebServices
{
    partial class frmTestWS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbUsername = new System.Windows.Forms.ComboBox();
            this.butValidateLogin = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.accessionTab = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.txtQohUnit = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtQoh = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtInventoryId = new System.Windows.Forms.TextBox();
            this.butChangeQoh = new System.Windows.Forms.Button();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.txtNewNumber = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtAccessionId3 = new System.Windows.Forms.TextBox();
            this.butChangeAccNumber = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.butSaveInventory = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.txtPolicy = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtinvNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAccessionId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvType = new System.Windows.Forms.TextBox();
            this.txtInvPrefix = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTaxon = new System.Windows.Forms.TextBox();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.butSaveData = new System.Windows.Forms.Button();
            this.listTab = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.txtFullPath3 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtTabName5 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.butDeleteItem = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtAccessionId2 = new System.Windows.Forms.TextBox();
            this.txtFullPath2 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtTabName4 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.butAddItemToList = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.butRenameListName = new System.Windows.Forms.Button();
            this.txtNewNameList = new System.Windows.Forms.TextBox();
            this.txtFullPath1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTabName3 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.butAddList = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtFullPath = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTabName2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.butDeleteList = new System.Windows.Forms.Button();
            this.searchTab = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtSearchInventory = new System.Windows.Forms.TextBox();
            this.butSearchInventory = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtSearchAccession2 = new System.Windows.Forms.TextBox();
            this.butSearchByName = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtSearchAccession3 = new System.Windows.Forms.TextBox();
            this.butSearchByTaxonomy = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSearchAccession1 = new System.Windows.Forms.TextBox();
            this.butSearchByNumber = new System.Windows.Forms.Button();
            this.otherTab = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cbLanguage = new System.Windows.Forms.ComboBox();
            this.butChangeLanguage = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCurrTabName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNewTabName = new System.Windows.Forms.TextBox();
            this.butRenameTab = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.butChangePassword = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butAddTab = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTabName = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.accessionTab.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.listTab.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.searchTab.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.otherTab.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pictureBox2);
            this.groupBox5.Controls.Add(this.pictureBox1);
            this.groupBox5.Controls.Add(this.cbUsername);
            this.groupBox5.Controls.Add(this.butValidateLogin);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.txtUserId);
            this.groupBox5.Controls.Add(this.txtPassword);
            this.groupBox5.Controls.Add(this.txtUrl);
            this.groupBox5.Location = new System.Drawing.Point(12, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(653, 137);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Login";
            // 
            // cbUsername
            // 
            this.cbUsername.FormattingEnabled = true;
            this.cbUsername.Items.AddRange(new object[] {
            "administrator",
            "e.rojas@cgiar.org"});
            this.cbUsername.Location = new System.Drawing.Point(139, 44);
            this.cbUsername.Name = "cbUsername";
            this.cbUsername.Size = new System.Drawing.Size(136, 21);
            this.cbUsername.TabIndex = 1;
            // 
            // butValidateLogin
            // 
            this.butValidateLogin.Location = new System.Drawing.Point(307, 71);
            this.butValidateLogin.Name = "butValidateLogin";
            this.butValidateLogin.Size = new System.Drawing.Size(120, 23);
            this.butValidateLogin.TabIndex = 4;
            this.butValidateLogin.Text = "Validate Login";
            this.butValidateLogin.UseVisualStyleBackColor = true;
            this.butValidateLogin.Click += new System.EventHandler(this.butValidateLogin_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Cooperator Id";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Password";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Username";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "webServiceURL";
            // 
            // txtUserId
            // 
            this.txtUserId.Enabled = false;
            this.txtUserId.Location = new System.Drawing.Point(139, 97);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.ReadOnly = true;
            this.txtUserId.Size = new System.Drawing.Size(77, 20);
            this.txtUserId.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(139, 71);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(136, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(139, 19);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(288, 20);
            this.txtUrl.TabIndex = 0;
            this.txtUrl.Text = "http://ggcommunity.cloudapp.net/gringlobal/gui.asmx";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.accessionTab);
            this.tabControl1.Controls.Add(this.listTab);
            this.tabControl1.Controls.Add(this.searchTab);
            this.tabControl1.Controls.Add(this.otherTab);
            this.tabControl1.Enabled = false;
            this.tabControl1.Location = new System.Drawing.Point(12, 156);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(657, 349);
            this.tabControl1.TabIndex = 35;
            // 
            // accessionTab
            // 
            this.accessionTab.Controls.Add(this.groupBox17);
            this.accessionTab.Controls.Add(this.groupBox16);
            this.accessionTab.Controls.Add(this.groupBox3);
            this.accessionTab.Controls.Add(this.groupBox2);
            this.accessionTab.Location = new System.Drawing.Point(4, 22);
            this.accessionTab.Name = "accessionTab";
            this.accessionTab.Padding = new System.Windows.Forms.Padding(3);
            this.accessionTab.Size = new System.Drawing.Size(649, 323);
            this.accessionTab.TabIndex = 0;
            this.accessionTab.Text = "Accession and Inventory";
            this.accessionTab.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.txtQohUnit);
            this.groupBox17.Controls.Add(this.label5);
            this.groupBox17.Controls.Add(this.txtQoh);
            this.groupBox17.Controls.Add(this.label14);
            this.groupBox17.Controls.Add(this.label38);
            this.groupBox17.Controls.Add(this.txtInventoryId);
            this.groupBox17.Controls.Add(this.butChangeQoh);
            this.groupBox17.Location = new System.Drawing.Point(334, 175);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(309, 134);
            this.groupBox17.TabIndex = 19;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Edit Quantity on Hand";
            // 
            // txtQohUnit
            // 
            this.txtQohUnit.Location = new System.Drawing.Point(173, 80);
            this.txtQohUnit.Name = "txtQohUnit";
            this.txtQohUnit.Size = new System.Drawing.Size(100, 20);
            this.txtQohUnit.TabIndex = 12;
            this.txtQohUnit.Text = "ct";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(39, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 27);
            this.label5.TabIndex = 13;
            this.label5.Text = "Quantity on hand units Id (string)";
            // 
            // txtQoh
            // 
            this.txtQoh.Location = new System.Drawing.Point(173, 47);
            this.txtQoh.Name = "txtQoh";
            this.txtQoh.Size = new System.Drawing.Size(100, 20);
            this.txtQoh.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(39, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Quantity on hand (integer)";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(39, 26);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(104, 13);
            this.label38.TabIndex = 10;
            this.label38.Text = "Inventory Id (integer)";
            // 
            // txtInventoryId
            // 
            this.txtInventoryId.Location = new System.Drawing.Point(173, 22);
            this.txtInventoryId.Name = "txtInventoryId";
            this.txtInventoryId.Size = new System.Drawing.Size(100, 20);
            this.txtInventoryId.TabIndex = 7;
            // 
            // butChangeQoh
            // 
            this.butChangeQoh.Location = new System.Drawing.Point(42, 107);
            this.butChangeQoh.Name = "butChangeQoh";
            this.butChangeQoh.Size = new System.Drawing.Size(231, 23);
            this.butChangeQoh.TabIndex = 9;
            this.butChangeQoh.Text = "Edit and Save";
            this.butChangeQoh.UseVisualStyleBackColor = true;
            this.butChangeQoh.Click += new System.EventHandler(this.butChangeQoh_Click);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.txtNewNumber);
            this.groupBox16.Controls.Add(this.label39);
            this.groupBox16.Controls.Add(this.label40);
            this.groupBox16.Controls.Add(this.txtAccessionId3);
            this.groupBox16.Controls.Add(this.butChangeAccNumber);
            this.groupBox16.Location = new System.Drawing.Point(18, 162);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(309, 147);
            this.groupBox16.TabIndex = 18;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Edit Accession Number";
            // 
            // txtNewNumber
            // 
            this.txtNewNumber.Location = new System.Drawing.Point(173, 59);
            this.txtNewNumber.Name = "txtNewNumber";
            this.txtNewNumber.Size = new System.Drawing.Size(100, 20);
            this.txtNewNumber.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(39, 51);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(107, 30);
            this.label39.TabIndex = 11;
            this.label39.Text = "New accession number (integer)";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(39, 34);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(107, 13);
            this.label40.TabIndex = 10;
            this.label40.Text = "Accession Id (integer)";
            // 
            // txtAccessionId3
            // 
            this.txtAccessionId3.Location = new System.Drawing.Point(173, 34);
            this.txtAccessionId3.Name = "txtAccessionId3";
            this.txtAccessionId3.Size = new System.Drawing.Size(100, 20);
            this.txtAccessionId3.TabIndex = 7;
            // 
            // butChangeAccNumber
            // 
            this.butChangeAccNumber.Location = new System.Drawing.Point(42, 107);
            this.butChangeAccNumber.Name = "butChangeAccNumber";
            this.butChangeAccNumber.Size = new System.Drawing.Size(231, 23);
            this.butChangeAccNumber.TabIndex = 9;
            this.butChangeAccNumber.Text = "Edit and Save";
            this.butChangeAccNumber.UseVisualStyleBackColor = true;
            this.butChangeAccNumber.Click += new System.EventHandler(this.butChangeAccNumber_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.butSaveInventory);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.txtPolicy);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtinvNumber);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtAccessionId);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtInvType);
            this.groupBox3.Controls.Add(this.txtInvPrefix);
            this.groupBox3.Location = new System.Drawing.Point(333, 9);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(309, 160);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Add Inventory";
            // 
            // butSaveInventory
            // 
            this.butSaveInventory.Location = new System.Drawing.Point(38, 131);
            this.butSaveInventory.Name = "butSaveInventory";
            this.butSaveInventory.Size = new System.Drawing.Size(231, 23);
            this.butSaveInventory.TabIndex = 11;
            this.butSaveInventory.Text = "Add New and Save";
            this.butSaveInventory.UseVisualStyleBackColor = true;
            this.butSaveInventory.Click += new System.EventHandler(this.butSaveInventory_Click);
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(33, 103);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(129, 39);
            this.label41.TabIndex = 17;
            this.label41.Text = "Inventory Maintenance Policy (integer)";
            // 
            // txtPolicy
            // 
            this.txtPolicy.Location = new System.Drawing.Point(168, 108);
            this.txtPolicy.Name = "txtPolicy";
            this.txtPolicy.Size = new System.Drawing.Size(100, 20);
            this.txtPolicy.TabIndex = 16;
            this.txtPolicy.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Inventory Number (integer)";
            // 
            // txtinvNumber
            // 
            this.txtinvNumber.Location = new System.Drawing.Point(169, 36);
            this.txtinvNumber.Name = "txtinvNumber";
            this.txtinvNumber.Size = new System.Drawing.Size(100, 20);
            this.txtinvNumber.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(35, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Accession Id (Integer)";
            // 
            // txtAccessionId
            // 
            this.txtAccessionId.Location = new System.Drawing.Point(169, 84);
            this.txtAccessionId.Name = "txtAccessionId";
            this.txtAccessionId.Size = new System.Drawing.Size(100, 20);
            this.txtAccessionId.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Inventory Type (string)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(35, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Inventory Prefix (string)";
            // 
            // txtInvType
            // 
            this.txtInvType.Location = new System.Drawing.Point(169, 60);
            this.txtInvType.Name = "txtInvType";
            this.txtInvType.Size = new System.Drawing.Size(100, 20);
            this.txtInvType.TabIndex = 9;
            this.txtInvType.Text = "SD";
            // 
            // txtInvPrefix
            // 
            this.txtInvPrefix.Location = new System.Drawing.Point(169, 12);
            this.txtInvPrefix.Name = "txtInvPrefix";
            this.txtInvPrefix.Size = new System.Drawing.Size(100, 20);
            this.txtInvPrefix.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtNumber);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtTaxon);
            this.groupBox2.Controls.Add(this.txtPrefix);
            this.groupBox2.Controls.Add(this.butSaveData);
            this.groupBox2.Location = new System.Drawing.Point(18, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(309, 147);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Add Accession";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(39, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Accession Number (integer)";
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(173, 48);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(100, 20);
            this.txtNumber.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(39, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Taxon (integer)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Accession Prefix (string)";
            // 
            // txtTaxon
            // 
            this.txtTaxon.Location = new System.Drawing.Point(173, 74);
            this.txtTaxon.Name = "txtTaxon";
            this.txtTaxon.Size = new System.Drawing.Size(100, 20);
            this.txtTaxon.TabIndex = 9;
            this.txtTaxon.Text = "36";
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(173, 22);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(100, 20);
            this.txtPrefix.TabIndex = 8;
            // 
            // butSaveData
            // 
            this.butSaveData.Location = new System.Drawing.Point(42, 110);
            this.butSaveData.Name = "butSaveData";
            this.butSaveData.Size = new System.Drawing.Size(231, 23);
            this.butSaveData.TabIndex = 11;
            this.butSaveData.Text = "Add New and Save";
            this.butSaveData.UseVisualStyleBackColor = true;
            this.butSaveData.Click += new System.EventHandler(this.butSaveData_Click);
            // 
            // listTab
            // 
            this.listTab.Controls.Add(this.groupBox7);
            this.listTab.Controls.Add(this.groupBox11);
            this.listTab.Controls.Add(this.groupBox10);
            this.listTab.Controls.Add(this.groupBox9);
            this.listTab.Location = new System.Drawing.Point(4, 22);
            this.listTab.Name = "listTab";
            this.listTab.Padding = new System.Windows.Forms.Padding(3);
            this.listTab.Size = new System.Drawing.Size(649, 323);
            this.listTab.TabIndex = 1;
            this.listTab.Text = "Lists and Items";
            this.listTab.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtItemName);
            this.groupBox7.Controls.Add(this.txtFullPath3);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.txtTabName5);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.butDeleteItem);
            this.groupBox7.Location = new System.Drawing.Point(333, 162);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(310, 147);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Delete Item";
            // 
            // txtItemName
            // 
            this.txtItemName.Location = new System.Drawing.Point(147, 82);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(151, 20);
            this.txtItemName.TabIndex = 3;
            // 
            // txtFullPath3
            // 
            this.txtFullPath3.Location = new System.Drawing.Point(147, 53);
            this.txtFullPath3.Name = "txtFullPath3";
            this.txtFullPath3.Size = new System.Drawing.Size(151, 20);
            this.txtFullPath3.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(12, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(137, 27);
            this.label27.TabIndex = 33;
            this.label27.Text = "example :\r\nTab 1 Root Folder|New List";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(12, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 13);
            this.label28.TabIndex = 32;
            this.label28.Text = "Item Name (string)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(12, 23);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(88, 13);
            this.label31.TabIndex = 30;
            this.label31.Text = "Tab Name (string)";
            // 
            // txtTabName5
            // 
            this.txtTabName5.Location = new System.Drawing.Point(147, 19);
            this.txtTabName5.Name = "txtTabName5";
            this.txtTabName5.Size = new System.Drawing.Size(151, 20);
            this.txtTabName5.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(12, 43);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 13);
            this.label32.TabIndex = 28;
            this.label32.Text = "Full path (string)";
            // 
            // butDeleteItem
            // 
            this.butDeleteItem.Location = new System.Drawing.Point(15, 108);
            this.butDeleteItem.Name = "butDeleteItem";
            this.butDeleteItem.Size = new System.Drawing.Size(283, 23);
            this.butDeleteItem.TabIndex = 4;
            this.butDeleteItem.Text = "Delete Item";
            this.butDeleteItem.UseVisualStyleBackColor = true;
            this.butDeleteItem.Click += new System.EventHandler(this.butDeleteItem_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtAccessionId2);
            this.groupBox11.Controls.Add(this.txtFullPath2);
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Controls.Add(this.label22);
            this.groupBox11.Controls.Add(this.label24);
            this.groupBox11.Controls.Add(this.txtTabName4);
            this.groupBox11.Controls.Add(this.label25);
            this.groupBox11.Controls.Add(this.butAddItemToList);
            this.groupBox11.Location = new System.Drawing.Point(333, 9);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(310, 147);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "New Item";
            // 
            // txtAccessionId2
            // 
            this.txtAccessionId2.Location = new System.Drawing.Point(147, 82);
            this.txtAccessionId2.Name = "txtAccessionId2";
            this.txtAccessionId2.Size = new System.Drawing.Size(151, 20);
            this.txtAccessionId2.TabIndex = 3;
            // 
            // txtFullPath2
            // 
            this.txtFullPath2.Location = new System.Drawing.Point(147, 53);
            this.txtFullPath2.Name = "txtFullPath2";
            this.txtFullPath2.Size = new System.Drawing.Size(151, 20);
            this.txtFullPath2.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(12, 57);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(137, 27);
            this.label29.TabIndex = 33;
            this.label29.Text = "example :\r\nTab 1 Root Folder|New List";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(12, 85);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 13);
            this.label22.TabIndex = 32;
            this.label22.Text = "Accession ID (integer)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(12, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 13);
            this.label24.TabIndex = 30;
            this.label24.Text = "Tab Name (string)";
            // 
            // txtTabName4
            // 
            this.txtTabName4.Location = new System.Drawing.Point(147, 19);
            this.txtTabName4.Name = "txtTabName4";
            this.txtTabName4.Size = new System.Drawing.Size(151, 20);
            this.txtTabName4.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(12, 43);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(81, 13);
            this.label25.TabIndex = 28;
            this.label25.Text = "Full path (string)";
            // 
            // butAddItemToList
            // 
            this.butAddItemToList.Location = new System.Drawing.Point(15, 108);
            this.butAddItemToList.Name = "butAddItemToList";
            this.butAddItemToList.Size = new System.Drawing.Size(283, 23);
            this.butAddItemToList.TabIndex = 4;
            this.butAddItemToList.Text = "New Item";
            this.butAddItemToList.UseVisualStyleBackColor = true;
            this.butAddItemToList.Click += new System.EventHandler(this.butAddItemToList_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.butRenameListName);
            this.groupBox10.Controls.Add(this.txtNewNameList);
            this.groupBox10.Controls.Add(this.txtFullPath1);
            this.groupBox10.Controls.Add(this.label30);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.txtTabName3);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.butAddList);
            this.groupBox10.Location = new System.Drawing.Point(18, 9);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(309, 147);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "New and Rename List";
            // 
            // butRenameListName
            // 
            this.butRenameListName.Location = new System.Drawing.Point(165, 109);
            this.butRenameListName.Name = "butRenameListName";
            this.butRenameListName.Size = new System.Drawing.Size(125, 23);
            this.butRenameListName.TabIndex = 5;
            this.butRenameListName.Text = "Rename List";
            this.butRenameListName.UseVisualStyleBackColor = true;
            this.butRenameListName.Click += new System.EventHandler(this.butRenameListName_Click);
            // 
            // txtNewNameList
            // 
            this.txtNewNameList.Location = new System.Drawing.Point(160, 83);
            this.txtNewNameList.Name = "txtNewNameList";
            this.txtNewNameList.Size = new System.Drawing.Size(129, 20);
            this.txtNewNameList.TabIndex = 3;
            // 
            // txtFullPath1
            // 
            this.txtFullPath1.Location = new System.Drawing.Point(161, 49);
            this.txtFullPath1.Name = "txtFullPath1";
            this.txtFullPath1.Size = new System.Drawing.Size(129, 20);
            this.txtFullPath1.TabIndex = 2;
            // 
            // label30
            // 
            this.label30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(26, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(137, 27);
            this.label30.TabIndex = 34;
            this.label30.Text = "example :\r\nTab 1 Root Folder|New List";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(26, 87);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(106, 13);
            this.label23.TabIndex = 32;
            this.label23.Text = "New list name (string)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(88, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "Tab Name (string)";
            // 
            // txtTabName3
            // 
            this.txtTabName3.Location = new System.Drawing.Point(161, 19);
            this.txtTabName3.Name = "txtTabName3";
            this.txtTabName3.Size = new System.Drawing.Size(129, 20);
            this.txtTabName3.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 40);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 28;
            this.label21.Text = "Full path (string)";
            // 
            // butAddList
            // 
            this.butAddList.Location = new System.Drawing.Point(28, 109);
            this.butAddList.Name = "butAddList";
            this.butAddList.Size = new System.Drawing.Size(125, 23);
            this.butAddList.TabIndex = 4;
            this.butAddList.Text = "New List";
            this.butAddList.UseVisualStyleBackColor = true;
            this.butAddList.Click += new System.EventHandler(this.butAddList_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtFullPath);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.txtTabName2);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.butDeleteList);
            this.groupBox9.Location = new System.Drawing.Point(18, 162);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(309, 147);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Delete List";
            // 
            // txtFullPath
            // 
            this.txtFullPath.Location = new System.Drawing.Point(161, 53);
            this.txtFullPath.Name = "txtFullPath";
            this.txtFullPath.Size = new System.Drawing.Size(129, 20);
            this.txtFullPath.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(26, 57);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(137, 27);
            this.label26.TabIndex = 35;
            this.label26.Text = "example :\r\nTab 1 Root Folder|New List";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(26, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "Tab Name (string)";
            // 
            // txtTabName2
            // 
            this.txtTabName2.Location = new System.Drawing.Point(161, 19);
            this.txtTabName2.Name = "txtTabName2";
            this.txtTabName2.Size = new System.Drawing.Size(129, 20);
            this.txtTabName2.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(26, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "Full path (string)";
            // 
            // butDeleteList
            // 
            this.butDeleteList.Location = new System.Drawing.Point(28, 108);
            this.butDeleteList.Name = "butDeleteList";
            this.butDeleteList.Size = new System.Drawing.Size(261, 23);
            this.butDeleteList.TabIndex = 3;
            this.butDeleteList.Text = "Delete List";
            this.butDeleteList.UseVisualStyleBackColor = true;
            this.butDeleteList.Click += new System.EventHandler(this.butDeleteList_Click);
            // 
            // searchTab
            // 
            this.searchTab.Controls.Add(this.groupBox14);
            this.searchTab.Controls.Add(this.groupBox13);
            this.searchTab.Controls.Add(this.groupBox15);
            this.searchTab.Controls.Add(this.groupBox12);
            this.searchTab.Location = new System.Drawing.Point(4, 22);
            this.searchTab.Name = "searchTab";
            this.searchTab.Size = new System.Drawing.Size(649, 323);
            this.searchTab.TabIndex = 2;
            this.searchTab.Text = "Search";
            this.searchTab.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label35);
            this.groupBox14.Controls.Add(this.txtSearchInventory);
            this.groupBox14.Controls.Add(this.butSearchInventory);
            this.groupBox14.Location = new System.Drawing.Point(333, 162);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(309, 147);
            this.groupBox14.TabIndex = 4;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Search Inventory";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(36, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(107, 13);
            this.label35.TabIndex = 13;
            this.label35.Text = "Accession Id (integer)";
            // 
            // txtSearchInventory
            // 
            this.txtSearchInventory.Location = new System.Drawing.Point(170, 44);
            this.txtSearchInventory.Name = "txtSearchInventory";
            this.txtSearchInventory.Size = new System.Drawing.Size(100, 20);
            this.txtSearchInventory.TabIndex = 1;
            // 
            // butSearchInventory
            // 
            this.butSearchInventory.Location = new System.Drawing.Point(39, 95);
            this.butSearchInventory.Name = "butSearchInventory";
            this.butSearchInventory.Size = new System.Drawing.Size(231, 23);
            this.butSearchInventory.TabIndex = 2;
            this.butSearchInventory.Text = "Search";
            this.butSearchInventory.UseVisualStyleBackColor = true;
            this.butSearchInventory.Click += new System.EventHandler(this.butSearchInventory_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label33);
            this.groupBox13.Controls.Add(this.txtSearchAccession2);
            this.groupBox13.Controls.Add(this.butSearchByName);
            this.groupBox13.Location = new System.Drawing.Point(18, 162);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(309, 147);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Search Accession";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(38, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(118, 13);
            this.label33.TabIndex = 13;
            this.label33.Text = "Accession Name (string)";
            // 
            // txtSearchAccession2
            // 
            this.txtSearchAccession2.Location = new System.Drawing.Point(172, 44);
            this.txtSearchAccession2.Name = "txtSearchAccession2";
            this.txtSearchAccession2.Size = new System.Drawing.Size(100, 20);
            this.txtSearchAccession2.TabIndex = 1;
            // 
            // butSearchByName
            // 
            this.butSearchByName.Location = new System.Drawing.Point(41, 95);
            this.butSearchByName.Name = "butSearchByName";
            this.butSearchByName.Size = new System.Drawing.Size(231, 23);
            this.butSearchByName.TabIndex = 2;
            this.butSearchByName.Text = "Search";
            this.butSearchByName.UseVisualStyleBackColor = true;
            this.butSearchByName.Click += new System.EventHandler(this.butSearchByName_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label36);
            this.groupBox15.Controls.Add(this.txtSearchAccession3);
            this.groupBox15.Controls.Add(this.butSearchByTaxonomy);
            this.groupBox15.Location = new System.Drawing.Point(333, 9);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(309, 147);
            this.groupBox15.TabIndex = 2;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Search Accession";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(36, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(107, 13);
            this.label36.TabIndex = 13;
            this.label36.Text = "Taxonomy Id (integer)";
            // 
            // txtSearchAccession3
            // 
            this.txtSearchAccession3.Location = new System.Drawing.Point(170, 44);
            this.txtSearchAccession3.Name = "txtSearchAccession3";
            this.txtSearchAccession3.Size = new System.Drawing.Size(100, 20);
            this.txtSearchAccession3.TabIndex = 1;
            // 
            // butSearchByTaxonomy
            // 
            this.butSearchByTaxonomy.Location = new System.Drawing.Point(39, 95);
            this.butSearchByTaxonomy.Name = "butSearchByTaxonomy";
            this.butSearchByTaxonomy.Size = new System.Drawing.Size(231, 23);
            this.butSearchByTaxonomy.TabIndex = 2;
            this.butSearchByTaxonomy.Text = "Search";
            this.butSearchByTaxonomy.UseVisualStyleBackColor = true;
            this.butSearchByTaxonomy.Click += new System.EventHandler(this.butSearchByTaxonomy_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label34);
            this.groupBox12.Controls.Add(this.txtSearchAccession1);
            this.groupBox12.Controls.Add(this.butSearchByNumber);
            this.groupBox12.Location = new System.Drawing.Point(18, 9);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(309, 147);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Search Accession";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(38, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(134, 13);
            this.label34.TabIndex = 13;
            this.label34.Text = "Accession Number (integer)";
            // 
            // txtSearchAccession1
            // 
            this.txtSearchAccession1.Location = new System.Drawing.Point(172, 44);
            this.txtSearchAccession1.Name = "txtSearchAccession1";
            this.txtSearchAccession1.Size = new System.Drawing.Size(100, 20);
            this.txtSearchAccession1.TabIndex = 1;
            // 
            // butSearchByNumber
            // 
            this.butSearchByNumber.Location = new System.Drawing.Point(41, 95);
            this.butSearchByNumber.Name = "butSearchByNumber";
            this.butSearchByNumber.Size = new System.Drawing.Size(231, 23);
            this.butSearchByNumber.TabIndex = 2;
            this.butSearchByNumber.Text = "Search";
            this.butSearchByNumber.UseVisualStyleBackColor = true;
            this.butSearchByNumber.Click += new System.EventHandler(this.butSearchByNumber_Click);
            // 
            // otherTab
            // 
            this.otherTab.Controls.Add(this.groupBox4);
            this.otherTab.Controls.Add(this.groupBox8);
            this.otherTab.Controls.Add(this.groupBox6);
            this.otherTab.Controls.Add(this.groupBox1);
            this.otherTab.Location = new System.Drawing.Point(4, 22);
            this.otherTab.Name = "otherTab";
            this.otherTab.Size = new System.Drawing.Size(649, 323);
            this.otherTab.TabIndex = 3;
            this.otherTab.Text = "Other";
            this.otherTab.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.cbLanguage);
            this.groupBox4.Controls.Add(this.butChangeLanguage);
            this.groupBox4.Location = new System.Drawing.Point(333, 162);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(309, 147);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Change Language";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(36, 44);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(89, 16);
            this.label37.TabIndex = 27;
            this.label37.Text = "List of languages";
            // 
            // cbLanguage
            // 
            this.cbLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLanguage.FormattingEnabled = true;
            this.cbLanguage.Items.AddRange(new object[] {
            "English",
            "Español",
            "Francés"});
            this.cbLanguage.Location = new System.Drawing.Point(146, 40);
            this.cbLanguage.Name = "cbLanguage";
            this.cbLanguage.Size = new System.Drawing.Size(100, 21);
            this.cbLanguage.TabIndex = 1;
            // 
            // butChangeLanguage
            // 
            this.butChangeLanguage.Location = new System.Drawing.Point(39, 103);
            this.butChangeLanguage.Name = "butChangeLanguage";
            this.butChangeLanguage.Size = new System.Drawing.Size(207, 23);
            this.butChangeLanguage.TabIndex = 2;
            this.butChangeLanguage.Text = "Change Language";
            this.butChangeLanguage.UseVisualStyleBackColor = true;
            this.butChangeLanguage.Click += new System.EventHandler(this.butChangeLanguage_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.txtCurrTabName);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.txtNewTabName);
            this.groupBox8.Controls.Add(this.butRenameTab);
            this.groupBox8.Location = new System.Drawing.Point(18, 162);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(309, 147);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Rename Tab";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(44, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 32);
            this.label17.TabIndex = 26;
            this.label17.Text = "Current Tab Name (string)";
            // 
            // txtCurrTabName
            // 
            this.txtCurrTabName.Location = new System.Drawing.Point(153, 34);
            this.txtCurrTabName.Name = "txtCurrTabName";
            this.txtCurrTabName.Size = new System.Drawing.Size(100, 20);
            this.txtCurrTabName.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(44, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 30);
            this.label16.TabIndex = 24;
            this.label16.Text = "New Tab Name (string)";
            // 
            // txtNewTabName
            // 
            this.txtNewTabName.Location = new System.Drawing.Point(153, 66);
            this.txtNewTabName.Name = "txtNewTabName";
            this.txtNewTabName.Size = new System.Drawing.Size(100, 20);
            this.txtNewTabName.TabIndex = 2;
            // 
            // butRenameTab
            // 
            this.butRenameTab.Location = new System.Drawing.Point(47, 103);
            this.butRenameTab.Name = "butRenameTab";
            this.butRenameTab.Size = new System.Drawing.Size(206, 23);
            this.butRenameTab.TabIndex = 3;
            this.butRenameTab.Text = "Rename Tab";
            this.butRenameTab.UseVisualStyleBackColor = true;
            this.butRenameTab.Click += new System.EventHandler(this.butRenameTab_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtNewPassword);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.butChangePassword);
            this.groupBox6.Location = new System.Drawing.Point(333, 9);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(309, 147);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Change Password";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(146, 45);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.Size = new System.Drawing.Size(100, 20);
            this.txtNewPassword.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(37, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(111, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "New Password (string)";
            // 
            // butChangePassword
            // 
            this.butChangePassword.Location = new System.Drawing.Point(40, 90);
            this.butChangePassword.Name = "butChangePassword";
            this.butChangePassword.Size = new System.Drawing.Size(206, 23);
            this.butChangePassword.TabIndex = 2;
            this.butChangePassword.Text = "Change Password";
            this.butChangePassword.UseVisualStyleBackColor = true;
            this.butChangePassword.Click += new System.EventHandler(this.butChangePassword_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butAddTab);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtTabName);
            this.groupBox1.Location = new System.Drawing.Point(18, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 147);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Tab";
            // 
            // butAddTab
            // 
            this.butAddTab.Location = new System.Drawing.Point(47, 90);
            this.butAddTab.Name = "butAddTab";
            this.butAddTab.Size = new System.Drawing.Size(206, 23);
            this.butAddTab.TabIndex = 2;
            this.butAddTab.Text = "Add Tab";
            this.butAddTab.UseVisualStyleBackColor = true;
            this.butAddTab.Click += new System.EventHandler(this.butAddTab_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Tab Name (string)";
            // 
            // txtTabName
            // 
            this.txtTabName.Location = new System.Drawing.Point(153, 45);
            this.txtTabName.Name = "txtTabName";
            this.txtTabName.Size = new System.Drawing.Size(100, 20);
            this.txtTabName.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::wfaTestWebServices.Properties.Resources.trust;
            this.pictureBox2.Location = new System.Drawing.Point(531, 22);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(115, 73);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::wfaTestWebServices.Properties.Resources.CIP;
            this.pictureBox1.Location = new System.Drawing.Point(443, 14);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(82, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // frmTestWS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 511);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox5);
            this.Name = "frmTestWS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test GRIN-Global Webservices";
            this.Load += new System.EventHandler(this.frmTestWS_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.accessionTab.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.listTab.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.searchTab.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.otherTab.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage accessionTab;
        private System.Windows.Forms.TabPage listTab;
        private System.Windows.Forms.TabPage searchTab;
        private System.Windows.Forms.TabPage otherTab;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtinvNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAccessionId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInvType;
        private System.Windows.Forms.TextBox txtInvPrefix;
        private System.Windows.Forms.Button butSaveInventory;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTaxon;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.Button butSaveData;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtNewNameList;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtTabName3;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtFullPath1;
        private System.Windows.Forms.Button butAddList;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTabName2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtFullPath;
        private System.Windows.Forms.Button butDeleteList;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbLanguage;
        private System.Windows.Forms.Button butChangeLanguage;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCurrTabName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtNewTabName;
        private System.Windows.Forms.Button butRenameTab;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.Button butChangePassword;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button butAddTab;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTabName;
        private System.Windows.Forms.Button butValidateLogin;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button butRenameListName;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.TextBox txtFullPath3;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtTabName5;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button butDeleteItem;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtAccessionId2;
        private System.Windows.Forms.TextBox txtFullPath2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtTabName4;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button butAddItemToList;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtSearchAccession2;
        private System.Windows.Forms.Button butSearchByName;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtSearchAccession1;
        private System.Windows.Forms.Button butSearchByNumber;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtSearchInventory;
        private System.Windows.Forms.Button butSearchInventory;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtSearchAccession3;
        private System.Windows.Forms.Button butSearchByTaxonomy;
        private System.Windows.Forms.ComboBox cbUsername;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox txtNewNumber;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtAccessionId3;
        private System.Windows.Forms.Button butChangeAccNumber;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox txtQoh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtInventoryId;
        private System.Windows.Forms.Button butChangeQoh;
        private System.Windows.Forms.TextBox txtQohUnit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtPolicy;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

